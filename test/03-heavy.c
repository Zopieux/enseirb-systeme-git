#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "thread.h"

/* test avec thread qui prend du temps pour tester la preemption */
static long heavy;

static void * thfunc(void *dummy __attribute__((unused)))
{
  unsigned long i;
  for(i = 0; i < heavy; i++) {
  }
  thread_exit(NULL);
}

int main(int argc, char *argv[])
{
  thread_t th;
  int err, i, nb;
  void *res;

  if (argc < 2) {
    printf("argument manquant: nombre de threads\n");
    return -1;
  }

  nb = atoi(argv[1]);
  heavy = atoi(argv[2]);

  for(i=0; i<nb; i++) {
    err = thread_create(&th, thfunc, NULL);
    assert(!err);
  }

  for(i=0; i<nb; i++) {
    err = thread_join(th, &res);
    assert(!err);
    assert(res == NULL);
  }

  printf("%d threads créés et détruits\n", nb);
  return 0;
}
