#!/bin/bash

MYPGM=$1
PTHREADPGM=$2

shift
shift

# is valgrind happy?
result="$(/usr/bin/valgrind --leak-check=full ./$MYPGM $@ 2>&1)"
echo "$result" | grep -iE "total heap usage: ([0-9]+) allocs, (\1) frees" >/dev/null 2>&1
noleakcode="$?"
echo "$result" | grep -i "0 errors from 0 contexts" >/dev/null 2>&1
noerrorcode="$?"

if [ $noleakcode -eq 0 ]; then
	echo -e "\\033[1;32mNO MEMORY LEAK\\033[0;39m"
	if [ $noerrorcode -eq 0 ]; then
		echo -e "\\033[1;32mNO VALGRIND ERROR\\033[0;39m"
	else
		echo -e "\\033[1;33mVALGRIND ERRORS\\033[0;39m"
	fi
else
	echo -e "\\033[1;33mMEMORY LEAK\\033[0;39m"
fi

# does it looks like pthread version?
/usr/bin/diff \
	<("./$MYPGM" $@ 2>/dev/null | sed 's/0x[0-9A-F]\+//gi' | sort) \
	<("./$PTHREADPGM" $@ 2>/dev/null | sed 's/0x[0-9A-F]\+//gi' | sort)

exit 0
