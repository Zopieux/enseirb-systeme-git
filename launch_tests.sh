#!/bin/bash

if [ -z $1 ]; then
	echo "Usage: $0 <build dir>"
	exit 1
fi

for exname in $(find ./bin -type f -executable -not -name '*-pthread'); do
	exname=$(basename $exname)
	echo -e "\\033[1;34mTest de $exname\\033[0;39m"
	make --quiet -C "$1" validate-$exname

	echo -e "\\033[1;34mPressez une touche pour passer au test suivant.\\033[0;39m"
	read -r -n 1 c
done
