\tableofcontents

\section*{Introduction}
La finalité de ce projet est d'implémenter un clone de la bibliothèque \emph{pthread} en espace utilisateur.
Dans un premier temps, à l'issue de ce rapport, nous avons réalisé une bibliothèque permettant la création de threads
coopératifs (sans préemption): c'est l'utilisateur qui doit manuellement effectuer un \code{thread_yield} pour passer la main
au suivant.

\section{Développement}

\subsection{Organisation du code}
Nous utilisons la suite d'outils CMake pour automatiser la compilation de la bibliothèque et des exemples fournis. Pour compiler, se placer dans la racine du projet, créer un dossier \code{build} et s'y rendre. Exécuter \code{cmake ..} puis \code{make}. Les binaires sont placés dans le dossier \code{bin}. Il y en a deux pour chaque exemple: un qui est compilé en utilisant notre bibliothèque, l'autre (suffixe \code{-pthread}) compilé avec pthread.

Chaque exemple peut être testé en appelant: \code{make validate-[nom]}, par exemple \code{make validate-01-main}. Cette commande effectue les opérations suivantes:
\begin{itemize}
\item Lancement du programme dans Valgrind en mode \emph{full memory check}, en cas de fuite mémoire \code{MEMORY LEAK} est affiché;
\item La sortie de notre programme et de l'équivalent pthread est nettoyé des adresses hexadécimales et passé dans l'utilitaire \code{diff} qui compare les deux sorties; un affichage vide signifie que les programmes se comportent de la même façon, à ordonnancement des lignes près.
\end{itemize}

\subsection{Initialisation et libération de la bibliothèque}
Notre code devant se comporter comme une bibliothèque, nous n'avons pas accès au \code{main} du client qui permettrait de faire les initialisations nécessaires au bon fonctionnement de notre projet. Ainsi, l'intégralité des fonctions publiques de notre librairie commencent par un appel à une fonction d'initialisation qui, une fois seulement, fera les constructions nécessaires.

De la même manière, pour libérer certaines ressources lors de la sortie du programme, nous avons enregistré un callback avec \code{atexit()}.

\subsection{Utilisation de la \emph{BSD queue}}
Pour stocker la liste des threads en cours, nous avons initialement tenté d'utiliser la bibliothèque de liste CCAN proposée dans le sujet; malheureusement l'intégration de cette bibliothèque n'était pas simple du fait de ses nombreuses dépendances. Aussi, nous nous sommes dirigés vers les \emph{BSQ queues}, beaucoup plus simples à intégrer: il ne s'agit que de macros. Son fonctionnement est un peu obscur mais son utilisation n'est pas compliquée si l'on s'en réfère correctement à la documentation.
Pour ce projet, nous utilisons une \code{CIRCLEQ}, qui implémente une queue circulaire. Le début et la fin de la queue sont liés, ce qui nous libère de la gestion du suivant du dernier élément. La documentation stipule que cette implémentation est 45 \% plus lente que les listes chaînées classiques, cependant cette différence est négligeable dans la mesure où nous n'utilisons en pratique qu'un petit nombre de threads, de l'ordre de la centaine.

\subsection{Structure \code{thread}}

\begin{verbatim}
struct thread {
    // identifiant du thread
    int id;

    // pointeur vers la fonction executée par le thread
    void *(*func_ptr)(void *);

    // argument de la fonction (un seul, cf. énoncé)
    void *func_argv;

    // valeur de retour de la fonction
    void *retval;

    // contexte du thread
    ucontext_t context;

    // début de la pile allouée
    int *stack_ptr;

    // identifiant de la pile pour que Valgrind ne nous embête pas
    int stack_id;

    // exécution terminée ?
    char is_done;

    // structure de queue BSD
    CIRCLEQ_ENTRY(thread) QUEUE_NEXT;
};
\end{verbatim}

Le premier champ sert d'identifiant unique au thread, son utilisation n'est pour l'instant qu'indicative. Les deux champs suivants permettent de stocker la fonction appelée lorsqu'on exécute le thread ainsi que ses arguments. Stocker ces deux champs ici permet de passer uniquement la structure \code{thread} au \emph{wrapper}, ce qui rend le code plus modulaire.

Le champ \code{retval} permet de sauvegarder la valeur de retour pour que le thread appelant \code{thread_join} puisse la récupérer \emph{a posteriori}.

Le champ \code{context} permet de sauvegarder le contexte, c'est-à-dire l'état du thread lorsque celui-ci perd la main.

Le champ \code{stack_ptr} permet de garder en mémoire le début de la pile que l'on alloue pour le contexte. Sans ce champ, nous ne pouvons pas libérer la mémoire de la pile proprement puisque nous perdons le début de celle-ci.

Le champ \code{stack_id} va contenir un identifiant de \emph{stack Valgrind} pour que ce dernier ne nous affiche pas d'erreur au moment des lectures/écritures dans la pile du contexte.

Le champ \code{is_done}, initialement \textsc{faux}, est mis à \textsc{vrai} lorsque \code{thread_exit} est appelée. En effet, nous avons pour l'instant fait le choix de conserver les threads dans la queue même s'ils terminent.

Le dernier champ de notre structure est nécessaire afin de pouvoir utiliser une \emph{BSD queue}.

\subsection{Cas du thread principal}
Le \code{main()} n'est pas spécialement considéré comme un cas à part. Sa structure \code{thread} est créée dans la fonction d'initialisation et le thread est ajouté à la queue comme tous les autres. La différence est que nous ne lui allouons pas de pile, puisque c'est déjà celle du programme.

La seconde différence se situe dans \code{thread_exit}: dans le cas du \emph{main thread}, nous attendons que tous les autres threads aient complété avant de libérer la mémoire et quitter.

\section{Tests et performances}


Tous les tests demandés sont passés avec succès. Un script permet entre autres de comparer les sorties selon que l'on utilise notre bibliothèque ou pthread.

Pour comparer les performances des deux bibliothèques, nous avons principalement utilisés les exécutables \code{switch-many} et \code{fibonacci}.
Sur la figure \ref{fig:plot-switch-threads} et la figure \ref{fig:plot-switch-yields} on constate que notre bibliothèque est plus efficace que pthread, cela est surtout du au fait que pour pthread les coûts d'ordonnancement sont beaucoup plus élevés.

Par ailleurs, on observe que pour notre bibliothèque, l'évolution du temps d'exécution en fonction du nombre de thread créés (ou du nombre d'appels à yield) est linéaire.
En revanche, pthread affiche un comportement beaucoup plus chaotique quand on atteint de grandes valeurs. Dans tous les cas, l'\emph{overhead} est plus important pour pthread; c'est notamment flagrant pour Fibonacci où l'on atteint des temps non raisonnables dès $n=16$ avec pthread alors que l'on peut facilement calculer $n=29$ avec notre version.

\begin{figure}[ht]
\includegraphics[width=1\textwidth]{img/plot-switchmany-threads}
\caption{Temps d'exécution de \code{31-switch-many} avec 10 yields en fonction du nombre de threads}
\label{fig:plot-switch-threads}
\end{figure}

\begin{figure}[ht]
\includegraphics[width=1\textwidth]{img/plot-switchmany-yields}
\caption{Temps d'exécution de \code{31-switch-many} avec 10 threads en fonction du nombre de yields}
\label{fig:plot-switch-yields}
\end{figure}

\begin{figure}[ht]
\includegraphics[width=1\textwidth]{img/plot-fibo}
\caption{Temps d'exécution de \code{51-fibonacci} en fonction du terme de la suite calculé}
\label{fig:plot-fibo}
\end{figure}
