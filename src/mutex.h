#include <stdio.h>
#include <stdlib.h>
#include "thread.h"
#include "queue.h"

#define LOCK 1
#define UNLOCK 0

typedef struct my_mutex * mutex_t;

struct my_mutex{
  //Booléen indiquant si le verrou est fermé
  int locked;
  //Liste contenant les threads bloqués par le verrou
  TAILQ_ENTRY(thread) QUEUE_NEXT;
};

int mutex_init(mutex_t *mutex);

int mutex_destroy(mutex_t *mutex);

int mutex_lock(mutex_t *mutex);

int mutex_unlock(mutex_t *mutex);
