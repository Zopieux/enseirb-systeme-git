#include "mutex.h"

// la lockqueue
static TAILQ_HEAD(QUEUE_HNAME, thread) lockqueue = TAILQ_HEAD_INITIALIZER(lockqueue);
  
  
int mutex_init(mutex_t *mutex){

  mutex = (mutex_t*) malloc(sizeof(mutex_t));
  if (mutex == NULL)
    return EXIT_FAILURE;
  
  (*mutex)->locked = UNLOCK;
  TAILQ_INIT(&lockqueue);

  return EXIT_SUCCESS;
}


int mutex_destroy(mutex_t *mutex){
  //La lockqueue est-elle vide?


  //Libère le mutex
  free((mutex_t*) mutex);
  return EXIT_SUCCESS;
}

int mutex_lock(mutex_t *mutex){
  #ifdef USE_PTHREAD
  pthread_t self = pthread_self();
  #endif
  thread_t self = thread_self();
  
  while(1){
    //Utilisation de __sync_val_compare_and_swap = Spécifique à gcc
    //Retourne UNLOCK = 1 si le mutex n'est pas encore pris, mutex lock
    if(__sync_val_compare_and_swap(&(*mutex)->locked, UNLOCK, LOCK))
      return EXIT_SUCCESS;

    if ((*mutex)->locked == UNLOCK){
      (*mutex)->locked = LOCK;
      return EXIT_SUCCESS;
    }

    //Sinon on entre dans la lockqueue
    else{
      //Ajoute à la lockqueue le thread qui a voulu accéder à la zone critique
      TAILQ_INSERT_TAIL(&lockqueue, self, QUEUE_NEXT);
      //Retire de la runqueue
      CIRCLEQ_REMOVE(&runqueue, self, QUEUE_NEXT);
      //Sauvegarde le changement de contexte à faire
      thread_t *tmp;
      //Premier thread locked
      tmp = TAILQ_FIRST(&lockqueue); 
      //Rend la possession du mutex
      (*mutex)->locked = UNLOCK;
      
      swapcontext(tmp.context, self.context);
      
      //Revient ici quand quelqu'un unlock le mutex et réveille
    }  
  }

  return EXIT_FAILURE;
}

int mutex_unlock(mutex_t *mutex){
  thread_t self = thread_self();
  
  //Retire le lock, Utilisation de __sync_val_compare_and_swap spécilique à gcc
  __sync_val_compare_and_swap(&(*mutex)->locked, LOCK, UNLOCK);
  //La lockqueue est-elle vide?
  if(QUEUE_NEXT != NULL){
    //Remet dans la runqueue le premier thread locked
    CIRCLEQ_INSERT_HEAD(&runqueue, self, QUEUE_NEXT);
    //Retire de la lockqueue ce thread
    TAILQ_REMOVE(&lockqueue, self, QUEUE_NEXT);

  return EXIT_SUCCESS;
}
