#include "thread.h"
#include <assert.h>
#include <queue.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <valgrind/valgrind.h>
// #include <unistd.h>

#define QUEUE_NEXT qnext
#define QUEUE_HNAME thread_queue
#define STACK_SIZE (64 * 1024)

#define DEBUG
#define TRUE 1
#define FALSE 0

/* définition de la structure de thread */
struct thread {
	// id du thread (pas forcément utile)
	int id;
	// pointeur vers la fonction
	void *(*func_ptr)(void *);
	// argument de la fonction (un seul, cf. énoncé)
	void *func_argv;
	// valeur de retour
	void *retval;
	// contexte du thread
	ucontext_t context;
	// ucontext_t *context_prev;
	// début de la pile allouée (malloc)
	void *stack_ptr;
	int stack_id;
	// terminé ?
	char is_done;
	// thread qui m'attend, si existe
	thread_t waiting_thread;
	// structure de queue BSD
	CIRCLEQ_ENTRY(thread) QUEUE_NEXT;
};

static unsigned _thread_count = 0;
static char _lib_init_done = FALSE;

// la runqueue
static CIRCLEQ_HEAD(QUEUE_HNAME, thread) runqueue = CIRCLEQ_HEAD_INITIALIZER(runqueue);

// thread en cours d'exécution (thread_t*)
// /!\ FIXME quand on passe en multi-cœur
static thread_t current_thread;
// Main thread
static thread_t main_thread;
// Sauvegarde de la stack et du struct au cas où c'est un thread qui termine en dernier
static thread_t last_thread = NULL;
// static ucontext_t main_ctx;

static int _thread_schedule(void);
static int _thread_swap(thread_t, thread_t);
static thread_t _thread_new(void);
static void _thread_free(thread_t);
static void _thread_func_wrap(thread_t);
static void _thread_lib_destroy(void);
static void _thread_lib_init(void);

/*
 * ===================================================
 *                  FONCTIONS OUTILS
 * ===================================================
 */

static void _thread_lib_init(void) {
	if(_lib_init_done)
		return;
	_lib_init_done = TRUE;

	// libération des globales
	atexit(_thread_lib_destroy);

	CIRCLEQ_INIT(&runqueue);

	if(NULL == (main_thread = _thread_new())) {
		perror("_thread_lib_init");
		exit(EXIT_FAILURE);
	}

	getcontext(&(main_thread->context));

	#ifdef DEBUG
	fprintf(stderr, "I DONE THE INIT (%p)\n", main_thread);
	#endif
	current_thread = main_thread;
	CIRCLEQ_INSERT_TAIL(&runqueue, main_thread, QUEUE_NEXT);
}


static void _thread_lib_destroy(void) {
	if(last_thread != NULL) {
		_thread_free(last_thread);
	}

	free(main_thread);
	last_thread = NULL;
	main_thread = NULL;

	#ifdef DEBUG
	fprintf(stderr, "exited\n");
	#endif
}


static thread_t _thread_new(void) {
	thread_t nt;
	if(NULL == (nt = malloc(sizeof(*nt)))) {
		perror("_thread_new: malloc");
		return NULL;
	}
	nt->id = _thread_count++; // FIXME: not thread safe
	nt->context.uc_link = NULL;
	nt->func_argv = NULL;
	nt->func_ptr = NULL;
	nt->is_done = FALSE;
	nt->retval = NULL;
	nt->stack_ptr = NULL;
	nt->stack_id = 0;
	nt->waiting_thread = NULL;
	return nt;
}


static void _thread_func_wrap(thread_t th) {
	void *retval;
	retval = th->func_ptr(th->func_argv);
	// on appelle thread_exit au cas où func ne l'ait pas fait
	thread_exit(retval);
}


static int _thread_swap(thread_t old, thread_t new) {
	int rv = 0;

	assert(old != new);
	assert(!new->is_done);

	// on swap vraiment !
	current_thread = new;
	rv = swapcontext(&(old->context), &(new->context));

	if(rv != 0) {
		perror("_thread_swap");
	}
	return rv;
}


static int _thread_schedule(void) {
	// trouve le premier thread !is_done
	thread_t this = thread_self();
	thread_t next_thread = this;

	while(TRUE) {

		next_thread = CIRCLEQ_LOOP_NEXT(&runqueue, next_thread, QUEUE_NEXT);

		if(next_thread == this) {  // boucle
			#ifdef DEBUG
			fprintf(stderr, "one thread only, no need to swap\n");
			#endif
			return 0;
		}

		if(next_thread->is_done) {  // FIXME: suffisant ? fonction is_done() ?
			#ifdef DEBUG
			fprintf(stderr, "already done (%p) ! continue search\n", next_thread);
			#endif
			continue;
		}

		break;
	}

	return _thread_swap(this, next_thread);
}


static void _thread_free(thread_t th) {
	VALGRIND_STACK_DEREGISTER(th->stack_id);
	free(th->stack_ptr);
	free(th);
}

/*
 * ===================================================
 *            IMPLEMENTATION BIBLIOTHEQUE
 * ===================================================
 */

/* recuperer l'identifiant du thread courant.
 */
thread_t thread_self(void) {
	_thread_lib_init();

	return current_thread;
};


/* creer un nouveau thread qui va exécuter la fonction func avec l'argument funcarg.
 * renvoie 0 en cas de succès, -1 en cas d'erreur.
 */
int thread_create(thread_t *newthread, void *(*func)(void *), void *funcarg) {
	_thread_lib_init();

	thread_t nt;

	if(NULL == (nt = _thread_new())) {
		perror("thread_create: _thread_new");
		return -1;
	}
	*newthread = nt;

	void *stack;

	if(NULL == (stack = malloc(STACK_SIZE))) {
		perror("thread_create: malloc");
		return -1;
	}

	// remplissage du ucontext_t du nouveau thread
	getcontext(&(nt->context));

	// on relie le ss_sp à notre pile tout juste allouée
	nt->context.uc_stack.ss_sp = stack;
	nt->context.uc_stack.ss_size = STACK_SIZE;

	// on sauvegarde le début de pile
	nt->stack_ptr = stack;
	nt->stack_id = VALGRIND_STACK_REGISTER(nt->context.uc_stack.ss_sp,
		nt->context.uc_stack.ss_sp + nt->context.uc_stack.ss_size);

	// fonction, arguments
	nt->func_ptr = func;
	nt->func_argv = funcarg;

	CIRCLEQ_INSERT_TAIL(&runqueue, nt, QUEUE_NEXT);

	// création du contexte
	makecontext(
		&(nt->context),
		(void (*)(void))_thread_func_wrap, 1, nt
	);

	return thread_yield();
}


/* passer la main à un autre thread.
 */
int thread_yield(void) {
	_thread_lib_init();


	//	thread_t next_thread = this;
	#ifdef DEBUG
	thread_t this = thread_self();
	fprintf(stderr, "I YIELD (current %p)\n", this);
	#endif

	return _thread_schedule();
}


/* attendre la fin d'exécution d'un thread.
 * la valeur renvoyée par le thread est placée dans *retval.
 * si retval est NULL, la valeur de retour est ignorée.
 */
int thread_join(thread_t thread, void **retval) {
	_thread_lib_init();

	int rv = 0;
	thread_t self = thread_self();

	/*
	si thread terminé on peut retourner
	si non, on enlève self de la runqueue et on ajoute celui qui attend à thread
	*/
	while(!thread->is_done) {
		CIRCLEQ_REMOVE(&runqueue, self, QUEUE_NEXT);
		thread->waiting_thread = self;
		rv = _thread_schedule();
	}

	if(retval != NULL) {
		*retval = thread->retval;
	}

	// le main thread n'a pas de stack
	// on free le main atexit() uniquement, pour pouvoir free les autres dans ce
	// contexte et éviter les erreurs valgrind
	if(thread != main_thread) {
		_thread_free(thread);
	}

	#ifdef DEBUG
	fprintf(stderr, "RETVAL FOR JOIN %d\n", rv);
	#endif
	return rv;
}


/* terminer le thread courant en renvoyant la valeur de retour retval.
 * cette fonction ne retourne jamais.
 */
void thread_exit(void *retval) {
	_thread_lib_init();

	thread_t this = thread_self();
	// on sauvegarde le retour
	this->retval = retval;
	// on marque terminé
	assert(!this->is_done);
	this->is_done = TRUE;

	#ifdef DEBUG
	fprintf(stderr, "THE THREAD %p IS DONE\n", this);
	#endif

	CIRCLEQ_REMOVE(&runqueue, this, QUEUE_NEXT);

	if(this->waiting_thread != NULL) {
		CIRCLEQ_INSERT_TAIL(&runqueue, this->waiting_thread, QUEUE_NEXT);
		//_thread_swap(this, this->waiting_thread);
	}

	if(this == main_thread) {

		do {
			thread_yield();
		} while(!CIRCLEQ_EMPTY(&runqueue));

		#ifdef DEBUG
		fprintf(stderr, "MAIN CIRQLEQ EMPTY\n");
		#endif
		// atexit fera le free
		exit(0);

	} else {

		// FIXME: inutile ?
		if(CIRCLEQ_EMPTY(&runqueue)) {
			#ifdef DEBUG
			fprintf(stderr, "CIRCLEQ EMPTY I AM THE PTR %p\n", this);
			#endif
			// atexit fera le free (ou pas)
			last_thread = this;
			setcontext(&(main_thread->context));
			exit(0);
		}

		_thread_schedule();

	}

	// on devrait pas arriver ici
	assert(FALSE);
}
